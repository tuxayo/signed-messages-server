import signed_messages_server.config as config
from .database import Database
from .send_email import send

database = Database()


def main():
    last_month_messages = database.get_last_month_messages()
    last_month_validity_percentage = validity_percentage(last_month_messages)

    last_two_months_messages = database.get_last_two_months_messages()
    last_two_months_validity_percentage = validity_percentage(last_two_months_messages)

    if last_month_validity_percentage > last_two_months_validity_percentage:
        trend = 'POSITIVE'
    elif last_month_validity_percentage < last_two_months_validity_percentage:
        trend = 'NEGATIVE'
    else:
        trend = 'NULL'

    email_content = (
        'last_two_months_validity_percentage: ' + str(last_two_months_validity_percentage) +
        '\nlast_month_validity_percentage: ' + str(last_month_validity_percentage) +
        '\ntrend: ' + trend)
    print(email_content)
    send(subject='Message validity report, trend ' + trend,
         content=email_content,
         from_=config.EMAIL_FROM_ADDRESS,
         to=config.EMAIL_REPORT_RECIPIENT,
         user_agent='message_validity_reporter',
         login=config.EMAIL_SMPT_LOGIN,
         passphrase=config.EMAIL_SMPT_PASSPHRASE,
         server=config.EMAIL_SMPT_SERVER)
    print('report sent to ' + config.EMAIL_REPORT_RECIPIENT)


def validity_percentage(messages):
    list_validity_status = [message[2] for message in messages]
    validity_ratio = sum(list_validity_status)/len(list_validity_status)
    return validity_ratio*100

if __name__ == '__main__':
    main()
