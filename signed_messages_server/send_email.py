import smtplib

from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid


def send(subject, content, from_, to, user_agent,
         login, passphrase, server, port=587):
    """
    Sends a text email using a specified SMTP server.

    :param from_: Content of the From header.
    The _ in the variable name is because from is a Python keyword.
    """
    message = MIMEText(content)
    message['Subject'] = subject
    message['From'] = from_
    message['To'] = to
    message['Date'] = formatdate()
    message['Message-ID'] = make_msgid()
    message['User-Agent'] = user_agent

    server = smtplib.SMTP(server + ':' + str(port))
    server.starttls()
    server.login(login, passphrase)
    server.send_message(message)
    server.quit()
