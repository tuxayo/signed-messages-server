import os
import sqlite3
import datetime

# external module
import dateutil.relativedelta


class Database:
    """Stores the messages in a database"""
    def __init__(self):
        self.db_conn = sqlite3.connect(Database._make_db_path())
        cursor = self.db_conn.cursor()
        cursor.execute('''
        CREATE TABLE IF NOT EXISTS
        messages
        (date text,
        message test,
        is_valid integer)
        ''')
        cursor.close()
        # TODO the DB connection is never closed,
        # check what issues that could lead to

    def save(self, message, is_valid):
        current_datetime_str = str(datetime.datetime.now())
        insert_query = 'INSERT INTO messages VALUES (?, ?, ?)'
        values = (current_datetime_str, message, is_valid)
        cursor = self.db_conn.cursor()
        cursor.execute(insert_query, values)
        self.db_conn.commit()

    def get_last_month_messages(self):
        current_datetime = datetime.datetime.now()
        one_month_ago = current_datetime - dateutil.relativedelta.relativedelta(months=1)
        return self._get_messages_since(one_month_ago)

    def get_last_two_months_messages(self):
        current_datetime = datetime.datetime.now()
        two_month_ago = current_datetime - dateutil.relativedelta.relativedelta(months=2)
        return self._get_messages_since(two_month_ago)

    def _get_messages_since(self, date):
        date_quoted = '"' + str(date) + '"'
        select_last_month_messages = ('SELECT * FROM messages ' +
                                      'WHERE date > ' + date_quoted)
        cursor = self.db_conn.cursor()
        return cursor.execute(select_last_month_messages)

    @staticmethod
    def _make_db_path():
        this_module_dir = os.path.dirname(__file__)
        data_dir = os.path.join(this_module_dir, '..', 'data')
        return os.path.join(data_dir, 'messages.sqlite')
