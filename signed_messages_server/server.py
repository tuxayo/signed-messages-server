import socket
import json
import base64

# PyCrypto
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5

from .database import Database


HOST = ''  # Empty string is a symbolic name meaning all available interfaces
database = Database()


def main():
    listen_sock = listen()
    while True:
        client_sock = wait_for_client(listen_sock)
        handle_client(client_sock)
        client_sock.close()
    listen_sock.close()


def listen(port=7070):
    listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listen_sock.bind((HOST, port))
    listen_sock.listen(1)  # listen with a backlog of 1
    return listen_sock


def wait_for_client(listen_sock):
    print('SERVER: Waiting for client')
    client_sock, client_adress = listen_sock.accept()
    print('SERVER: Connected by', client_adress)
    return client_sock


def handle_client(conn_sock):
    raw_message = conn_sock.recv(1024).decode("utf-8")

    # I know that sending the public key with the message defeats the purpose
    # of signing but key distribution is beyond this project's scope.
    (message_content, signature, public_key_str) = unpack_message(raw_message)
    public_key = unserialize_public_key(public_key_str)
    is_valid = verify_signature(message_content,
                                signature,
                                public_key)
    if(is_valid):
        notify_purchasing_director()
        conn_sock.sendall(b'ACK')
    else:
        conn_sock.sendall(b'NACK')
    database.save(message_content, is_valid)

    conn_sock.close()


def unserialize_public_key(public_key_str):
    public_key_pem = ("-----BEGIN RSA PUBLIC KEY-----\n" +
                      public_key_str +
                      "\n-----END RSA PUBLIC KEY-----")
    public_key_bytes = bytes(public_key_pem, 'utf-8')
    return RSA.importKey(public_key_bytes)


def unpack_message(raw_message):
    message_dic = json.loads(raw_message)
    return (message_dic['message'],
            message_dic['signature'],
            message_dic['public_key'])


def verify_signature(message, signature, public_key):
    message_hash = SHA256.new(message.encode('utf-8'))
    verifier = PKCS1_v1_5.new(public_key)
    signature_bytes = base64.b64decode(signature)
    return verifier.verify(message_hash, signature_bytes)


def notify_purchasing_director():
    # This is actually an imaginary function.
    # Imagine that it notifies the director that a new purchase request has
    # been made. Then the director open it's client which fetches the message
    # from the database.
    pass  # this single instruction does all this, trust me ;-)

if __name__ == '__main__':
    main()
