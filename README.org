#+TITLE: Security course project: Signed messages: Server

[[https://git.framasoft.org/tuxayo/signed-messages-android-client][Android client]]

* Features planned/done
** Required
- [X] wait for a message and send an ACK
- [X] check the signature
- [X] calculate KPI of successfully check messages
** Important
- [X] send KPI by email
** Nice to have
** Maybe

* Dependencies
- Python 3
- PyCrypto
- dateutil

* Configuration
It's needed for the email reports, fill `signed_messages_server/config.py`
with your relevant settings.

* Usage
** Run the server
=python -m signed_messages_server.server=

** Generate report and send it by email
KPI: It calculates the percentage of valid messages of the last month and
the last two months. Compares then to make a trend, and send that by email.

=python -m signed_messages_server.do_reporting=

* Tools used (to easely find code examples later)
- Python 3
- sockets
- PyCrypto
- unserialize RSA public key
- verify RSA signed message (that was signed on Android)
- email sending with an SMTP server: login, uses starttls, all necessary headers
- SQLite

* License
AGPLv3+
